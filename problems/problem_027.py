# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    highest_value = values[0]
    for i in values:
        if i > highest_value:
            highest_value = i
    return(highest_value)

my_list = [1,5,2,3,8,6,541]
other_list = max_in_list(my_list)

print(other_list)